01. Crea una clase para representar a un alumno. Del alumno queremos guardar:

    Nombre
    Apellidos
    DNI
    Fecha de nacimiento
    Nota media primer ciclo
    Nota media segundo ciclo

Crea un constructor, los métodos getter de todos los atributos y los setter (validados) de los atributos que el programa debería dejar cambiar. A mayores, crea un método que devuelva el nombre completo, otro que devuelva la nota media del ciclo (si no se ha realizado algún ciclo, asumiremos que la nota es null). Sobreescribe toString() para que muestre DNI + nombre completo + Fecha nacimiento. Sobreescribe compareTo para que ordene por defecto a los estudiantes por DNI. Sobreescribe equals para que sea igual si coincide el atributo DNI diga que es el mismo alumno. Sobreescribe hashcode teniendo en cuenta esto.

Crea un programa que tenga el siguiente menú:

1. Crear alumno.
2. Modificar alumno (solicita DNI del alumno a modificar y si la nota a insertar es de primero o segundo).
3. Mostrar los alumnos ordenados de forma Natural (DNI).
