/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1;

import java.time.DateTimeException;
import java.time.LocalDate;
import org.daw1.classes.Alumno;

/**
 *
 * @author alumno
 */
public class Menu {

    private static java.util.Scanner teclado = new java.util.Scanner (System.in);
    private static java.util.Set<Alumno> alumnos;
    
    public static void main(String[] args){
        alumnos = new java.util.TreeSet<>();
        String entradaUsuario = "";
        do{
            
            System.out.println("*************************************");
            System.out.println("* 1. Alta alumno                    *");            
            System.out.println("* 2. Modificar notas alumno         *");
            System.out.println("* 3. Mostrar listado alumnos        *");
            System.out.println("*                                   *");           
            System.out.println("* 0. Salir                          *");            
            System.out.println("*************************************");   
            entradaUsuario = teclado.nextLine();
            switch(entradaUsuario){
                case "1":
                    crearAlumno();                    
                    break;
                case "2":
                    establecerNota();
                    break;
                case "3":
                    System.out.println(alumnos);
                    break;
                case "0":                    
                    break;
                default:
                    System.out.println("Entrada incorrecta");
            }        
            System.out.println(alumnos);
        }
        while(!entradaUsuario.equals("0"));
    }
    
    
    
    private static void test(){
        LocalDate dt = null;
        do{
            System.out.println("Año");
            int anho = 12;
            System.out.println("Mes");
            int mes = 12;
            System.out.println("dia");
            int dia = 34;
            try{
                dt = LocalDate.of(anho, mes, dia);
            }
            catch(DateTimeException ex){
                System.out.println("La fecha insertada no es válida");
            }
        }
        while(dt == null);
    }
    
    private static void crearAlumno(){
        String nombre = "";
        do{
            System.out.println("Inserte el nombre del alumno");
            nombre = teclado.nextLine();
        }while(nombre.isBlank());
        
        String apellidos = "";
        do{
            System.out.println("Inserte los apellidos del alumno");
            apellidos = teclado.nextLine();
        }while(nombre.isBlank());
        
        String dni = "";
        do{
            System.out.println("Por favor inserte el DNI del alumno");
            dni = teclado.nextLine();
        }
        while(dni.isEmpty() || !dni.matches(Alumno.FORMATO_DNI));
        
        LocalDate fechaNacimiento = null;
        do{
            int anho = 9999;
            do{
                System.out.println("Por favor inserte el año de nacimiento");
                if(teclado.hasNextInt()){
                    anho = teclado.nextInt();
                }
                teclado.nextLine();
            }while(anho > LocalDate.now().get(java.time.temporal.ChronoField.YEAR));
            
            int mes = 0;
            do{
                System.out.println("Por favor inserte el mes de nacimiento");
                if(teclado.hasNextInt()){
                    mes = teclado.nextInt();
                }
                teclado.nextLine();
            }while(mes < 1 || mes > 12);
            
            int dia = 0;
            do{
                System.out.println("Por favor inserte el día de nacimiento");
                if(teclado.hasNextInt()){
                    dia = teclado.nextInt();
                }
                teclado.nextLine();
            }while(dia < 1 || dia > 31);
            try{
                fechaNacimiento = LocalDate.of(anho, mes, dia);
            }
            catch(java.time.DateTimeException ex){
                System.out.println("La fecha insertada no es correcta");
            }
            
        }while(fechaNacimiento == null || fechaNacimiento.compareTo(LocalDate.now()) > 0);
        
        Alumno alumno = new Alumno(nombre, apellidos, dni, fechaNacimiento);
        if(!alumnos.add(alumno)){
            System.out.println("El alumno insertado ya existe en el sistema");
        }
        else{
            System.out.println("Alumno guardado con éxito");
        }
    }
    
    private static void establecerNota(){
        String dniBuscado = "";
        do{
            System.out.println("Inserte el DNI del alumno sobre el que quiere trabajar");
            dniBuscado = teclado.nextLine();
        }
        while(dniBuscado.isBlank() || !dniBuscado.matches(Alumno.FORMATO_DNI));
        
        java.util.Iterator<Alumno> it = alumnos.iterator();
        Alumno alumno = null;
        String ultimoElemento = "";
        while (it.hasNext() && dniBuscado.compareTo(ultimoElemento) > 0) {
            alumno = it.next();
            ultimoElemento = alumno.getDni();
        }
        if(dniBuscado.equals(ultimoElemento)){
            String opcion = "";
            do{
                System.out.println("¿Qué nota desea establecer?\n1. Primer ciclo\n2. Segundo ciclo");
                opcion = teclado.nextLine();
            }
            while(!opcion.equals("1") && !opcion.equals("2"));
            int nota = -1;
            do{
                System.out.println("Por favor inserte la nota que desea establecer");
                if(teclado.hasNextInt()){
                    nota = teclado.nextInt();
                }
            }
            while(nota < 0 || nota > 10);
            if(opcion.equals("1")){
                alumno.setNotaPrimerCiclo(nota);
            }
            else{
                alumno.setNotaSegundoCiclo(nota);
            }
            System.out.println("Datos actualizados del alumno:\n" + alumno);
            
        }
        else{
            System.out.println("El alumno seleccionado no existe");
        }        
    }
    
}
