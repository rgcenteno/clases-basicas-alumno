/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.classes;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Alumno implements Comparable<Alumno>{
    
    private String nombre;
    private String apellidos;
    private final String dni;
    private final LocalDate fechaNacimiento;
    private Integer notaPrimerCiclo = null;
    private Integer notaSegundoCiclo = null;
    
    public static final String FORMATO_DNI = "[0-9]{8}[A-Z]";
    
    public Alumno(String nombre, String apellidos, String dni, LocalDate fechaNac){
        checkDni(dni);
        checkFechaNacimiento(fechaNac);
        checkNotNull(nombre);
        checkNotNull(apellidos);
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.dni = dni;
        this.fechaNacimiento = fechaNac;    
    }
    
    /***********************************************************************************************/

    public void setNombre(String nombre){
        checkNotNull(nombre);
        this.nombre = nombre;
    }
    
    public void setApellidos(String apellidos){
        checkNotNull(apellidos);
        this.apellidos = apellidos;
    }
    
    public void setNotaPrimerCiclo(Integer nota){
        Alumno.checkNota(nota);
        this.notaPrimerCiclo = nota;   
    }
    
    public void setNotaSegundoCiclo(Integer nota){
        Alumno.checkNota(nota);
        this.notaSegundoCiclo = nota;   
    }
    
    /***********************************************************************************************/
    
    public String getNombre(){
        return this.nombre;
    }
    
    public String getApellidos(){
        return this.apellidos;
    }
    
    public String getDni(){
        return this.dni;
    }
    
    public LocalDate getFechaNacimiento(){
        return this.fechaNacimiento;
    }
    
    public Integer getNotaPrimerCiclo(){
        return this.notaPrimerCiclo;
    }
    
    public Integer getNotaSegundoCiclo(){
        return this.notaSegundoCiclo;
    }
    
    /**
     * Devuelve el nombre completo resultado de concatenar nombre y apellidos
     * @return Nombre completo resultado de concatenar nombre y apellidos
     */
    
    public String getNombreCompleto(){
        return this.nombre + " " + this.apellidos;
    }
    
    /**
     * Calcula la media del ciclo. En caso de que no tengamos alguna nota devuelve null
     * @return La media del ciclo. En caso de no tener nota en primero o en segundo devuelve null
     */
    
    public Double getNotaMedia(){
        if(this.notaPrimerCiclo == null || this.notaSegundoCiclo == null){
            return null;
        }
        else{
            return (double) (this.notaPrimerCiclo + this.notaSegundoCiclo) / 2;
        }
    
    }
    
    /***********************************************************************************************/
    
    /**
     * Suponemos que un DNI debe tener siempre 8 numeros y 1 letra
     * @param dni 
     */
    
    private static void checkDni(String dni){
        if(dni == null || !dni.matches(FORMATO_DNI)){
            throw new IllegalArgumentException("El DNI debe estar formado por 8 numeros seguidos de una letra");
        }
    }
    
    private static void checkNotNull(String cadena){
        if(cadena == null){
            throw new IllegalArgumentException("No se permiten valores nulos");
        }
    }
    
    private static void checkFechaNacimiento(LocalDate fechaNac){
        if(fechaNac == null || fechaNac.compareTo(LocalDate.now()) >= 0){
            throw new IllegalArgumentException("La fecha de nacimiento debe ser anterior a hoy");
        }
    }
    
    
    private static void checkNota(Integer nota){
        if(nota != null && (nota < 0 || nota > 10)){
            throw new IllegalArgumentException("La nota debe tener valores entre 0 y 10");
        }
    }
    
    /***********************************************************************************************/

    @Override
    public String toString(){
        String formatoFecha = "dd/MM/yyyy";
        java.time.format.DateTimeFormatter formateador = DateTimeFormatter.ofPattern(formatoFecha);
        String resultado = dni + "\n" + this.getNombreCompleto() + "\n" + formateador.format(fechaNacimiento);
        if(this.notaPrimerCiclo != null){
            resultado += "\n1er: " + this.notaPrimerCiclo +"";
        }
        if(this.notaSegundoCiclo != null){
            resultado += "\n2o: " + this.notaSegundoCiclo + "";
        }
        return resultado;
    }

    @Override
    public boolean equals(Object obj){
        if(obj == null){
            return false;
        }
        if(obj instanceof Alumno){
            return this.dni.equals(((Alumno)obj).getDni());
        }
        else{
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.dni.hashCode();
    }

    /**
     * Compara dos alumnos en base a su DNI
     * @param t Alumno a comparar
     * @return menor que 0 si t.getDni() es menor que this.getDni(), igual a 0  si t.getDni() es igual que this.getDni() y mayor que 0 en otro caso
     */
    @Override
    public int compareTo(Alumno t) {
        if(t == null){
            throw new NullPointerException();
        }
        else{
            return this.dni.compareTo(t.getDni());
        }
    }

    
}
